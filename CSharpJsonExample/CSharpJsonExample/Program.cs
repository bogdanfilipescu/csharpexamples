﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;


namespace CSharpJsonExample
{
    class Program
    {
        static void Main(string[] args)
        {
            JsonExample json_example = new JsonExample();
            string json_out = json_example.SerializeJSON();

            Console.WriteLine(json_out);

            json_example.DeserializeJSON(json_out);
        }
    }
    
    class JsonExample
    {
        public JsonExample()
        {
        }

        public string SerializeJSON()
        {
            Dictionary<string, object> cmd_data = new Dictionary<string, object>
            {
                { "addr", 0x1234 },
                { "data", 0x5678 },
            };

            Dictionary<string, object> header = new Dictionary<string, object>
            {
                { "api", "mama are mere" },
                { "seq-num", "tata are pere" }
            };


            Dictionary<string, Dictionary<string, object>> command = new Dictionary<string, Dictionary<string, object>>
            {
                { "header", header},
                { "data", cmd_data }
            };


            Console.WriteLine(command["header"]["api"]);
            Console.WriteLine(command["header"]["seq-num"]);
            Console.WriteLine(command["data"]["addr"]);
            Console.WriteLine(command["data"]["data"]);

            string jsonout = JsonSerializer.Serialize(command);
            
            return jsonout;
        }

        public void DeserializeJSON(string json_string)
        {
            Dictionary<string, Dictionary<string, object>> command = new Dictionary<string, Dictionary<string, object>>();
            command = JsonSerializer.Deserialize<Dictionary<string, Dictionary<string, object>>>(json_string);

            Console.WriteLine(command["header"]["api"]);
            Console.WriteLine(command["header"]["seq-num"]);
            Console.WriteLine(command["data"]["addr"]);
            Console.WriteLine(command["data"]["data"]);

        }
    }

}
