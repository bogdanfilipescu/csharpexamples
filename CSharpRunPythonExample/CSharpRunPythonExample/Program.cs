﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpRunPythonExample
{
    class Program
    {
        static RunPythonScriptService pyScriptService = new RunPythonScriptService();

        static void Main(string[] args)
        {
            try
            {
                pyScriptService.GetPythonScriptOutputLineByLineEvHandler = PythonScriptOutputLineByLineEvHandler;
                pyScriptService.GetPythonScriptStdErrLineByLineEvHandler = PythonScriptStdErrLineByLineEvHandler;
                pyScriptService.StartRunPythonScriptAsync(  @"D:\Projects\CSharpRunPythonExample\CSharpRunPythonExample\python_script.py",
                                                            new List<string> { "--arg1 1", "--arg2 2", "--arg3 3" }
                                                         );

                bool isFinished = pyScriptService.WaitUntilFinish(-1);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        private static void PythonScriptOutputLineByLineEvHandler(object sender, DataReceivedEventArgs e)
        {
            if(!string.IsNullOrEmpty(e.Data))
            {
                Console.WriteLine(e.Data);
            }
                
        }

        private static void PythonScriptStdErrLineByLineEvHandler(object sender, DataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Data))
            {
                Console.WriteLine(e.Data);               
            }

        }
    }


    class RunPythonScriptService
    {
        private string _pythonPath;
        public string PythonAppPath
        {
            set { _pythonPath = value; }
            private get { return _pythonPath; }
        }

        private Process _process;

        /// <summary>
        /// property for configuring the output line reading callback
        /// 
        /// event handler example
        ///     void PythonScriptOutputLineByLineEvHandler(object sender, DataReceivedEventArgs e)
        ///     {
        ///          //process e.Data 
        ///     }
        /// 
        /// observation:
        ///     the printed output from python script must be in a standard way to work realtime
        ///     ex: print(something,flush=True)
        /// </summary>
        public DataReceivedEventHandler GetPythonScriptOutputLineByLineEvHandler
        {
            set
            {
                if (_process != null)
                {
                    _process.OutputDataReceived += value;
                }
                else
                {
                    throw new Exception("object does not exist");
                }
            }
            private get
            {
                return null;
            }
        }

        /// <summary>
        /// property for configuring the std error output line reading callback
        /// 
        /// event handler example
        ///     void PythonScriptStdErrLineByLineEvHandler(object sender, DataReceivedEventArgs e)
        ///     {
        ///          //process e.Data 
        ///     }
        /// 
        /// observation:
        ///     the printed output from python script must be in a standard way to work realtime
        ///     ex: print(something,flush=True)
        /// </summary>
        public DataReceivedEventHandler GetPythonScriptStdErrLineByLineEvHandler
        {
            set
            {
                if (_process != null)
                {
                    _process.ErrorDataReceived += value;
                }
                else
                {
                    throw new Exception("object does not exist");
                }
            }
            private get
            {
                return null;
            }
        }

        /// <summary>
        /// Constructor
        /// creates a new process object and get the path of the python app
        /// </summary>
        public RunPythonScriptService()
        {
            _process = new Process();
            GetPythonPath();           
        }

        /// <summary>
        /// get windows path environment variable and search for the python (at least version 3) app path
        /// </summary>
        /// <returns></returns>
        private void GetPythonPath()
        {
            IDictionary environmentVariables = Environment.GetEnvironmentVariables();
            string pathVariable = environmentVariables["Path"] as string;
            string pythonAppPath = "";
            if (pathVariable != null)
            {
                string[] allPaths = pathVariable.Split(';');
                foreach (var path in allPaths)
                {
                    if(path.Contains(@"Python3"))
                    {
                        if (File.Exists(path + "python.exe"))
                        {
                            pythonAppPath =  path + "python.exe";
                        }
                    }                                           
                }
            }

            if(!pythonAppPath.Equals(""))
            {
                _pythonPath = pythonAppPath;
            }
             
        }

        /// <summary>
        /// - configures the process start info;
        /// - start the process;
        /// </summary>
        /// <param name="pythonScript"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public void StartRunPythonScriptAsync(string pythonScript, List<string> args)
        {

            System.Diagnostics.ProcessStartInfo pythonScriptProcessInfo = new System.Diagnostics.ProcessStartInfo();

            //python interprater location
            pythonScriptProcessInfo.FileName = this._pythonPath;

            //argument with file name and input parameters
            string pyScript = pythonScript;
            foreach (string arg in args)
            {
                pyScript += " " + arg;
            }
            pythonScriptProcessInfo.Arguments = pyScript;

            pythonScriptProcessInfo.UseShellExecute = false;        // Do not use OS shell
            pythonScriptProcessInfo.RedirectStandardOutput = true;  // Any output, generated by application will be redirected back
            pythonScriptProcessInfo.RedirectStandardError = true;   // Any error in standard output will be redirected back (for example exceptions)
            
            _process.StartInfo = pythonScriptProcessInfo;
            
            _process.Start();

            _process.BeginOutputReadLine();
            _process.BeginErrorReadLine();
        }

        /// <summary>
        /// block the current thread until the process is finished if the milisecondsTimeout=-1
        /// block the current thread until the process is finished or timeout if milisecondsTimeout >= 0
        /// </summary>
        /// <param name="milisecondsTimeout"></param>
        /// <returns></returns>
        public bool WaitUntilFinish(int milisecondsTimeout)
        {       
            if(milisecondsTimeout == -1)
            {
                _process.WaitForExit();
                return true;
            }
            else
            {
                return _process.WaitForExit(milisecondsTimeout);
            }            
        }
       
    }
}
