﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        private static TestCLass _testCLass;

        static void Main(string[] args)
        {
            _testCLass = new TestCLass();

            TestAsyncAwaitMethods();

            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
        }

        public static void TestAsyncAwaitMethods()
        {
            LongRunningMethod1();
            LongRunningMethod2();
            LongRunningMethod3();
            LongRunningMethod4(20);
        }

        public static async void LongRunningMethod1()
        {

            Action<object> action = (object obj) =>
            {
                Console.WriteLine("Starting Long Running method 1...");
                //Task.Delay(5000);
                System.Threading.Thread.Sleep(7000);
                Console.WriteLine("End Long Running method 1...");
            };

            Task t1 = new Task(action,1);
            t1.Start();

            await t1; 
        }

        public static async void LongRunningMethod2()
        {

            Action action = () =>
            {
                Console.WriteLine("Starting Long Running method 2...");
                System.Threading.Thread.Sleep(5000);
                Console.WriteLine("End Long Running method 2...");
            };

            await Task.Run(action);
        }

        public static async void LongRunningMethod3()
        {
            int x = 2;
            var task = new TaskFactory().StartNew( new Action(() => { LongRunningMethod3Exec(x); }));
            await task;
        }

        private static void LongRunningMethod3Exec(int x)
        {
            Console.WriteLine("Starting Long Running method 3..." + x.ToString());
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine("End Long Running method 3..." + x.ToString());
        }

        public static async void LongRunningMethod4(int param)
        {
            await Task.Run(() => 
            {
                _testCLass.TestMethood(param);
                Console.WriteLine("End LongRunningMethod4..." + param.ToString());
            });
            
        }


    }

    class TestCLass
    {
        private int _localAttribute;

        public TestCLass()
        {
            _localAttribute = 5;
        }

        public void TestMethood(int var_for_print)
        {
            var_for_print += 10;
            Console.WriteLine("Starting TestMethood...local_attribute " + _localAttribute.ToString() + " param " + var_for_print.ToString());
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("End TestMethood...local_attribute " + _localAttribute.ToString() + " param " + var_for_print.ToString());

        }
    }
}
